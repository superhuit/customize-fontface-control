import './style.scss'

import FontFaceFileSelector from '../font-face-file-selector/index'

const SELECTOR_LIST = '.supt-cffc__formats-list'
const SELECTOR_ITEM = '.supt-cffc__formats-item'
const SELECTOR_ADD_BTN = '.supt-cffc__formats-add-item'
const SELECTOR_REMOVE = '.supt-cffc__formats-remove'

const SELECTOR_ELEMENT_TO_UPDATE_ATTRS = 'label,input,select'
const SELECTOR_ELEMENT_TO_FOCUS = 'input,select'

const SELECTOR_FILE_FIELD = '.supt-cffc__file'

const ATTRS_TO_REPLACE = ['for', 'id', 'name']

interface RefsType {
	list: HTMLElement,
	addBtn: HTMLElement,
	tple: HTMLTemplateElement,
}

interface StateType {
	itemsCount: number,
}

interface PropsType {
	min: Number,
	max: Number,
	onChange: Function,
}

export interface FormatsRepeaterType {}
export class FormatsRepeater {
	el: HTMLElementAlt
	refs: RefsType
	props: PropsType
	state: StateType

	constructor(el: HTMLElementAlt, onChange: Function ) {
		this.el = el;

		this.refs = {
			list: this.el.querySelector(SELECTOR_LIST),
			addBtn: this.el.querySelector(SELECTOR_ADD_BTN),
			tple: this.el.querySelector('template'),
		};

		this.props = {
			min: this.el.dataset.min ? parseInt(this.el.dataset.min) : 0,
			max: this.el.dataset.max ? parseInt(this.el.dataset.max) : Number.MAX_SAFE_INTEGER,
			onChange: onChange,
		};

		this.state = {
			itemsCount: this.refs.list.childElementCount,
		};

		Array.from(this.refs.list.children)
			.forEach((item: HTMLElement) => this.initFileFields(item))

		this.checkMinMax();
		this.bindEvents();

		// @ts-ignore
		window._formatsRepeater = window._formatsRepeater || []
		// @ts-ignore
		window._formatsRepeater.push(this)
	}

	bindEvents() {
		this.onAddClick = this.onAddClick.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.updateAttrsId = this.updateAttrsId.bind(this)

		this.refs.addBtn.addEventListener('click', this.onAddClick);
		this.el.addDelegateListener('click', SELECTOR_REMOVE, this.onDeleteClick );
	}

	initFileFields( item: HTMLElement ) {
		Array
			.from(item.querySelectorAll(SELECTOR_FILE_FIELD))
			.forEach((fileEL:HTMLElement) => new FontFaceFileSelector(fileEL, { onChange: this.props.onChange } ))
	}

	onAddClick(event: Event) {
		event.preventDefault();

		const newItem: HTMLElement = <HTMLElement>this.refs.tple.content.cloneNode(true);
		this.updateAttrsId(newItem, this.state.itemsCount);

		const finalItem = <HTMLElement>newItem.firstElementChild;
		this.refs.list.appendChild(newItem);

		const formEl: HTMLInputElement = finalItem.querySelector(SELECTOR_ELEMENT_TO_FOCUS)
		formEl.focus();

		this.initFileFields(finalItem)

		this.state.itemsCount++;
		this.checkMinMax();
	}

	onDeleteClick(event: Event) {
		event.preventDefault();
		event.stopPropagation();

		const target: HTMLElement = <HTMLElement>event.target

		const item = target.closest(SELECTOR_ITEM);
		this.refs.list.removeChild(item);

		Array.from(this.refs.list.children).forEach( this.updateAttrsId );

		this.state.itemsCount--;
		this.checkMinMax();
	}

	updateAttrsId( item: HTMLElement, newId: number = 0 ) {
		Array
			.from(item.querySelectorAll(SELECTOR_ELEMENT_TO_UPDATE_ATTRS))
			.forEach((el: HTMLElement) => {
				ATTRS_TO_REPLACE.forEach((attr) => {
					if ( el.hasAttribute(attr) ) el.setAttribute(attr, el.getAttribute( attr ).replace(/(_|\[)\d+(_|\])/, `$1${newId}$2`));
				});
			});
	}

	checkMinMax() {
		this.el.classList[ this.state.itemsCount <= this.props.min ? 'add' : 'remove' ]('has-minimum-items');
		this.el.classList[ this.state.itemsCount >= this.props.max ? 'add' : 'remove' ]('has-maximum-items');
	}

	destroy() {
		this.refs.addBtn.removeEventListener('click', this.onAddClick);
	}
}
