import {FormatsRepeaterType, FormatsRepeater} from "../formats-repeater/index"

const SELECTOR_INNER      = '.supt-cffc__inner'
const SELECTOR_INPUTS     = 'input,select'
const SELECTOR_LINK_INPUT = '[data-customize-setting-link]'

interface RefsType {
	inner: HTMLFormElementAlt,
	input: HTMLInputElement,
	formats?: FormatsRepeaterType,
}

interface StateType {
	value: Object,
}

export default class CustomizerFontFaceControl {
	el
	state: StateType
	refs: RefsType

	constructor(el: HTMLElement) {
		this.el = el

		this.refs = {
			inner: this.el.querySelector(SELECTOR_INNER),
			input: this.el.querySelector(SELECTOR_LINK_INPUT),
		}

		let value;
		try { value = JSON.parse(this.refs.input.value) }
		catch { value = {} }

		this.state = {
			value,
		}

		this.onSubInputChange = this.onSubInputChange.bind(this);
		this.refs.inner.addDelegateListener('change', SELECTOR_INPUTS, this.onSubInputChange)

		this.refs.formats = new FormatsRepeater(this.el.querySelector('.supt-cffc__formats'), this.onSubInputChange)
	}

	onSubInputChange(event: Event) {
		this.refs.input.value = this.getFormInputJSON(this.refs.inner);
		this.refs.input.dispatchEvent(new Event('change'));
	}

	getFormInputJSON(form: HTMLFormElement): string {
		const data: FormData = new FormData(form)
		let object: any = {}

		data.forEach((value: string, key: string) => {
			const matches: any[] = Array.from(key.matchAll(/\[([^\]]+)\]/g));

			if ( Array.isArray(matches) && matches.length ) {
				this.keyPathToObject(object, matches.map((match) => match[1]), value)
			}
			else {
				object[key] = value
			}
		});

		return JSON.stringify(object);
	}

	keyPathToObject(obj: any, keyPath: Array<string>, value: any) {
		const lastKeyIndex: number = keyPath.length-1;

		for (var i = 0; i < lastKeyIndex; ++ i) {
			const key: string = keyPath[i];
			const nextKey: string = keyPath[i+1];

			if (!(key in obj)) {
				obj[key] = /\d+/.test(nextKey) ? [] : {}
			}
			obj = obj[key];
		}
		obj[keyPath[lastKeyIndex]] = value;
 	}
}

