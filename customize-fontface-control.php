<?php
/**
 * Plugin Name: Customize FontFace Control
 * Plugin URI:  https://gitlab.com/superhuit/customize-font-face-control
 * Description: WordPress Customizer's custom control to define a font face.
 * Author:      Felipe (@kuuak)
 * Author URI:  https://profiles.wordpress.org/kuuak/
 * Version:     1.0.0
 * License:     GNU General Public License v3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );
define( "PLUGIN_VERSION", "1.0.0" );

/**
 * Action & filter hooks
 */
add_action('init', 'supt_customize_fontface_control_register' );

function supt_customize_fontface_control_register() {

	if ( !class_exists('WP_Customize_Control') ) return;

	class SUPT_Customize_FontFace_Control extends WP_Customize_Control {
		public $type = 'fontface';

		public function enqueue() {
			wp_enqueue_style( 'supt_customize_fontface_control', plugins_url('dist/supt-customize-fontface-control.css', __FILE__), null, PLUGIN_VERSION );
			wp_enqueue_script( 'supt_customize_fontface_control', plugins_url('dist/supt-customize-fontface-control.js', __FILE__), null,  PLUGIN_VERSION, true);

			wp_enqueue_media();
		}

		public function render_content() {
			$value = $this->value();
			$this->parsedValue = json_decode($value, true);

			$data = [
				'%id'      => esc_attr( $this->id ),
				'%label'   => esc_html( $this->label ),
				'%desc'    => $this->get_description(),

				'%family'  => $this->get_family_field(),
				'%formats' => $this->get_rendered_format_list_field(),

				'%default' => (empty($value) ? '{}' : esc_attr($value)),
				'%link'    => $this->get_link()
			];

			echo str_replace(
				array_keys($data),
				array_values($data),
				'<fieldset class="supt-cffc" id="supt-cffc-%id" >
					<legend>%label</legend>
					%desc
					<form class="supt-cffc__inner">
						%family
						%formats
					</form>
					<input type="hidden" name="%id" value="%default" %link />
				</fieldset>'
			);
		}

		private function get_description() {
			return ( empty($this->description)
				? ''
				: sprintf( '<p>%s</p>', wp_kses_post( $this->description ) )
			);
		}

		private function get_family_field() {
			$data = [
				'%id'          => esc_attr( "{$this->id}_fontFamily" ),
				'%name'        => esc_attr( 'fontFamily' ),
				'%label'       => esc_html__( 'Font Family', 'supt-cffc' ),
				'%value'       => $this->parsedValue['fontFamily'] ?? '',
				'%placeholder' => esc_attr__( 'Verdana, Geneva, sans-serif', 'supt-cffc' ),
			];

			return str_replace(
				array_keys($data),
				array_values($data),
				'<div class="supt-cffc__field">
					<label for="%id">%label</label>
					<input type="text" id="%id" name="%name" value="%value" placeholder="%placeholder" />
				</div>'
			);
		}

		private function get_rendered_format_list_field() {

			if ( empty($this->parsedValue['formats']) )$this->parsedValue['formats'][] = null;
			$data = [
				'%label' => esc_html__( 'Font variants', 'supt-cffc' ),
				'%addBtn' => esc_html__( 'Add a variant', 'supt-cffc' ),

				'%formats' => implode("\n",
					array_map(function($i) {
						return $this->get_format_item_field($i, $this->parsedValue['formats'][$i] ?? null);
					}, array_keys($this->parsedValue['formats']) )
				),
				'%template' => $this->get_format_item_field(),
			];

			return str_replace(
				array_keys($data),
				array_values($data),
				'<div class="supt-cffc__field supt-cffc__formats">
					<div class="supt-cffc__formats-label">%label</div>
					<ol class="supt-cffc__formats-list">%formats</ol>
					<button class="supt-cffc__formats-add-item button button-primary button-small">%addBtn</button>
					<template>%template</template>
				</div>'
			);
		}

		private function get_format_item_field($i = 0, $item = null) {

			$fields[] = $this->get_select_field( $i, $item, "fontWeight", __('Font Weight', 'supt-cffc'), [
				'100' => __('Thin (Hairline)', 'supt-cffc'),
				'200' => __('Extra Light (Ultra Light)', 'supt-cffc'),
				'300' => __('Light', 'supt-cffc'),
				'400' => __('Normal (Regular)', 'supt-cffc'),
				'500' => __('Medium', 'supt-cffc'),
				'600' => __('Semi Bold (Demi Bold)', 'supt-cffc'),
				'700' => __('Bold', 'supt-cffc'),
				'800' => __('Extra Bold (Ultra Bold)', 'supt-cffc'),
				'900' => __('Black (Heavy)', 'supt-cffc'),
			] );

			$fields[] = $this->get_select_field( $i,$item, 'fontStyle',  __('Font Style', 'supt-cffc'), [
				'normal' => __('Normal', 'supt-cffc'),
				'italic' => __('Italic', 'supt-cffc'),
			] );

			$fields[] = $this->get_file_field( $i, $item, 'woff', __('WOFF file', 'supt-cffc') );
			$fields[] = $this->get_file_field( $i, $item, 'woff2', __('WOFF2 file', 'supt-cffc') );

			$data = [
				'%fields' => implode("\n", $fields),
				'%deleteBtn' => __('Supprimer', 'vdar'),
			];

			return str_replace(
				array_keys($data),
				array_values($data),
				'<li class="supt-cffc__formats-item">
					%fields
					<div class="supt-cffc__formats-item-remove">
						<button class="supt-cffc__formats-remove button button-link button-link-delete">%deleteBtn</button>
					</div>
				</li>'
			);
		}

		private function get_select_field($i, $item, $name, $label, $choices = []) {

			$value = $item[$name] ?? null;
			$opts = [];
			foreach ($choices as $optValue => $optLabel) {
				$opts[] = sprintf(
					'<option value="%1$s"%3$s>%2$s</option>',
					esc_attr($optValue),
					esc_html($optLabel),
					( $value == $optValue ? ' selected' : '')
				);
			}

			$data = [
				'%id'      => esc_attr("{$this->id}_{$i}_{$name}"),
				'%name'    => esc_attr("[formats][{$i}][{$name}]"),
				'%class'   => esc_attr("--$name"),
				'%label'   => esc_html($label),
				'%options' => implode("\n", $opts),
			];

			return str_replace(
				array_keys($data),
				array_values($data),
				'<div class="supt-cffc__field %class">
					<label for="%id">%label</label>
					<select id="%id" name="%name">
						%options
					</select>
				</div>'
			);
		}
		private function get_file_field($i, $item, $name, $label) {
			$file_data = [
				'%name'        => esc_attr( "[formats][{$i}][{$name}]" ),
				'%label'       => esc_html__( $label ),
				'%value'       => $item[$name] ?? '',
				'%btnTitle'    => esc_html__('Upload or select a file', 'supt-cffc'),
			];
			return str_replace(
				array_keys($file_data),
				array_values($file_data),
				'<div class="supt-cffc__field supt-cffc__file">
					<label>%label</label>
					<button type="button" class="upload-button button-add-media">%btnTitle</button>
					<input type="hidden" name="%name" value="%value" />
				</div>'
			);
		}
	}
}

function supt_customizer_fontface_get_setting( $setting_name ) {
	$setting = json_decode(get_theme_mod($setting_name, '{}'), true);

	if ( is_array($setting) && isset($setting['formats']) && is_array($setting['formats']) ) {
		$setting['formats'] = array_map(function($format) {

			if ( !empty($format['woff']) ) $format['woff'] = wp_get_attachment_url($format['woff']);
			if ( !empty($format['woff2']) ) $format['woff2'] = wp_get_attachment_url($format['woff2']);

			return $format;
		}, $setting['formats']);
	}

	return $setting;
}

