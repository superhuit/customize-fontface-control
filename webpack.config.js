const path = require('path');
const package = require('./package.json');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	stats: 'errors-only',
	devtool: 'cheap-eval-source-map',
	entry: {
		[package.name]: path.resolve(__dirname, 'src', 'index.ts'),
	},
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: '[name].js',
	},
	resolve: {
		extensions: [ '.ts', '.js' ],
	},
	plugins: [new MiniCssExtractPlugin({ 	filename: '[name].css', })],
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /.scss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{ loader: 'css-loader', options: { sourceMap: true , importLoaders: 1 }, },
					{
						loader: 'postcss-loader',

					},
				]
			}
		]
	},
	devServer: {
		contentBase: './dist',
		quiet: true,
		host: 'localhost',
		hot: true,
		port: 3000,
		open: false,
		overlay: true,
		stats: {
			colors: true
		},
		publicPath: 'http://localhost:3000/',
		historyApiFallback: false,
		compress: false,
		disableHostCheck: true,
	},
}
